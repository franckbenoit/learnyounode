var fs = require('fs'),
path = require('path'),
myDir = process.argv[2],
myExtension = '.' +  process.argv[3];

fs.readdir(myDir, function (error, list) {

    if (error) {
        console.log(error);
        return;
        
    } else {
        // better use forEach in node
        list.forEach (function (file) {

            if ( path.extname(file) === myExtension  ) {
                console.log( file );
            }
        });

    }
});
