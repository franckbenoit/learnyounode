var myModule = require('./reader'),
myDir = process.argv[2],
myExtension = process.argv[3];

myModule(myDir, myExtension, function (error, data) {

    if (error) {
        return console.error('there was an error: ', error);
    } 

    data.forEach(function (file) {
        console.log(file);
    });
    
});
