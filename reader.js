var fs = require('fs'),
path = require('path'),
myArray = new Array();

module.exports = function (myDir, myExtension, callback) {

    fs.readdir(myDir, function (error, list) {


        if (error) {
            callback(error);
        } else {


            // better use forEach in node
            // simple boucle avec forEach
            // list.forEach (function (file) {

            //     if ( path.extname(file) === '.' +  myExtension  ) {
            //         myArray.push(file);
            //     }
            // });
            

            //better : foo.filter(function);
            list = list.filter(function(index) {
                // return si vrai, et rempli un nouveau tablea
                return path.extname(index) === '.' + myExtension;
            });
            callback(null, list);
        }
    });    
}